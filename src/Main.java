package com.greatlearning.forkjoin;

import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) throws Exception {
         if(args.length !=1){
             throw new Exception("Invalid Input passed \nUsage :main <urlText> ");
         }
        String urlText = args[0];
        ForkJoinPool forkPool = new ForkJoinPool();
        UrlConnectionReader urlConnectionReader = new UrlConnectionReader(urlText);
        forkPool.invoke(urlConnectionReader);
        forkPool.shutdown();

    }
}



