package com.greatlearning.forkjoin;

import java.io.Closeable;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.concurrent.RecursiveTask;
import java.net.URL;


public class UrlConnectionReader extends RecursiveTask<String> implements Closeable {

    private static HttpURLConnection urlConnection;
    private static PrintWriter printWriter;
    private static  int MIN = 0;
    private static int MAX = 1500;
    private static int optimal =5000;
    private byte[] data;
    private byte[] streamData;


    public UrlConnectionReader(String urlText){
        try {


            URL url = new URL(urlText);
            System.out.println(url);
            this.urlConnection = (HttpURLConnection) url.openConnection();
            this.urlConnection.setRequestProperty("Content-Type","application/json");
            this.urlConnection.setDoOutput(true);

        printWriter=new PrintWriter(new FileOutputStream("output.text",true));
        data=new byte[1024];
        StringBuilder stringBuilder=new StringBuilder();

        int Read=0;
        Read=urlConnection.getInputStream().read(data,MIN,MAX);
        stringBuilder.append(new String(data));
        while (Read!=-1)
        {
            data=new byte[1024];
            Read=urlConnection.getInputStream().read(data,MIN,MAX);
            stringBuilder.append(new String(data));
        }

        streamData =stringBuilder.toString().getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public UrlConnectionReader(byte[] input)
    {
        this.streamData=input;
        System.out.println("Invoked:"+input.length);
    }
    @Override
    protected String compute()
    {
        System.out.println("welcome To Compute");
        System.out.println("Data length"+streamData.length);
        String line=null;
        if (streamData.length<optimal)
        {
            line=new String(streamData);

        }else {
            byte[] obj1=Arrays.copyOfRange(streamData,0,streamData.length/2);
            byte[] obj2=Arrays.copyOfRange(streamData,streamData.length/2,streamData.length);
            UrlConnectionReader task1=new UrlConnectionReader(obj1);
            UrlConnectionReader task2=new UrlConnectionReader(obj2);
            task1.fork();
            task2.fork();

            line= String.valueOf(task1.join());
            System.out.println("Line Of Task1:"+line);
            line=line.concat(String.valueOf(task2.join()));
            System.out.println("line of Task2:"+line);

        }
        printWriter.write(line);
        return line;
    }
    @Override
    public void close() throws IOException

    {
        printWriter.close();
    }


}
